from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

my_url= 'https://www.newegg.com/Product/ProductList.aspx?Submit=ENE&N=100007709%204814%20601201888%20601203793%20601204369%20601296707%20601301599&IsNodeId=1&bop=And&PageSize=12&order=RATING'

uClient = uReq(my_url)
page_html= uClient.read()
uClient.close()

page_soup= soup(page_html, "html.parser")

#print(page_soup.p)
containers= page_soup.findAll("div",{"class":"item-container"})
print(len(containers))

file_name="product.csv"
f=open(file_name,"w")

headers = "Brand, Product_Name, Rating, User_Rated, Shipping\n"

f.write(headers)

for container in containers:
#container=containers[0]

    brand=container.div.div.a.img["title"]
    print(brand)
    
    title_container=container.findAll("a",{"class":"item-title"})
    product_name = title_container[0].text
    print(product_name) 
    
    shipping_container=container.findAll('li',{"class":"price-ship"})
    shipping = shipping_container[0].text.strip()
    print(shipping) 
    
    
    rating_container=container.findAll('a',{"class":"item-rating"})
    rating = rating_container[0].text[1:-1]
    rating1 = rating_container[0]["title"][-1]
    print(rating1,rating) 
    f.write(brand+','+ product_name.replace(",",".")+ ',' +rating1+','+rating+','+shipping+'\n')

    print()
f.close()